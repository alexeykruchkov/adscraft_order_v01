$(document).ready(function(){

// content tabs
  $('.order-form-tabs li').on("click", function(){
    var tab_id = $(this).attr('data-tab');

    $('.order-form-tabs li').removeClass('active');
    $('.tab-content .tab-content__item').removeClass('active');

    $(this).addClass('active');
    $("#"+tab_id).addClass('active');
  });

// mobile nav
  $('.header-nav__mobile-btn').on("click", function(){
    console.log('asgsa');
    $(this).toggleClass('btn-active');
    $('.header-nav__list').toggle();
  });

// add upload block
  $('.append-bl__tip').on("click", function() {
    var elem = $(this).prev('.append-bl__item').clone(true);
    $(this).prev('.append-bl__item').after(elem);
  });

// filter-slides counter
  var itemsLenght = $('.filter-carousel .filter-carousel__item').length;
  

// banner carousel
  $('#carousel').slick({
    dots: false,
    adaptiveHeight: true
  });

// tab carousels
  $('.filter-carousel').slick({
    adaptiveHeight: true,
    dots: true,
    arrows: true
  });
  $('.filter-carousel .slick-dots').append('<div class="filter-slider__items-all"></div>');
  $('.filter-slider__items-all').html("/" + itemsLenght);

// Q&A toggle text
  $('.questions-bl__row-title').on("click", function () {
    $(this).parent('.questions-bl__row').toggleClass('row-expand');
  });

// testimonials carousel
  $('.testimonials-bl__carousel').slick({
    adaptiveHeight: true,
    dots: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          dots: true,
        }
      }
    ]
  });

});